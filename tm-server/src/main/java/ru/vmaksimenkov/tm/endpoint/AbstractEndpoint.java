package ru.vmaksimenkov.tm.endpoint;

import ru.vmaksimenkov.tm.api.service.ServiceLocator;

public class AbstractEndpoint {

    protected final ServiceLocator serviceLocator;

    AbstractEndpoint(final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

}
