package ru.vmaksimenkov.tm.exception.user;

import ru.vmaksimenkov.tm.exception.AbstractException;

public class AlreadyLoggedInException extends AbstractException {

    public AlreadyLoggedInException() {
        super("Error! You are already logged in...");
    }

}
