package ru.vmaksimenkov.tm.exception.user;

import ru.vmaksimenkov.tm.exception.AbstractException;

public class LoginExistsException extends AbstractException {

    public LoginExistsException() {
        super("Error! Login already exists...");
    }

}
