package ru.vmaksimenkov.tm.listener;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.jms.LogMessage;
import ru.vmaksimenkov.tm.service.ActiveMQConnectionService;

import javax.jms.Destination;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.persistence.*;

import static ru.vmaksimenkov.tm.constant.PropertyConst.SUBJECT;

public final class EntityListener {

    @NotNull
    private final ActiveMQConnectionService connectionService = new ActiveMQConnectionService();

    @PostLoad
    public void postLoad(@NotNull final Object record) {
        sendMessage(record, "LOAD");
    }

    @PrePersist
    public void prePersist(@NotNull final Object record) {
        sendMessage(record, "START PERSIST");
    }

    @PostPersist
    public void postPersist(@NotNull final Object record) {
        sendMessage(record, "FINISH PERSIST");
    }

    @PreUpdate
    public void preUpdate(@NotNull final Object record) {
        sendMessage(record, "START UPDATE");
    }

    @PostUpdate
    public void postUpdate(@NotNull final Object record) {
        sendMessage(record, "FINISH UPDATE");
    }

    @PreRemove
    public void preRemove(@NotNull final Object record) {
        sendMessage(record, "START REMOVE");
    }

    @PostRemove
    public void postRemove(@NotNull final Object record) {
        sendMessage(record, "FINISH REMOVE");
    }

    @SneakyThrows
    public void sendMessage(@NotNull final Object entity, @NotNull final String event) {
        @NotNull final Session session = connectionService.getConnection().createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Destination destination = session.createTopic(SUBJECT);
        @NotNull final MessageProducer producer = session.createProducer(destination);
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @Nullable final String json = mapper.writeValueAsString(entity);
        @NotNull final LogMessage logMessage = new LogMessage(entity.getClass().getSimpleName(), event, json);
        @NotNull final ObjectMessage message = session.createObjectMessage(logMessage);
        producer.send(message);
    }

}
