package ru.vmaksimenkov.tm.api.service;

public interface IActiveMQConnectionService {
    javax.jms.Connection getConnection();
}
