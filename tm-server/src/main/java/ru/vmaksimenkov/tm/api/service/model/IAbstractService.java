package ru.vmaksimenkov.tm.api.service.model;

import ru.vmaksimenkov.tm.api.repository.model.IAbstractRepository;
import ru.vmaksimenkov.tm.model.AbstractEntity;

public interface IAbstractService<E extends AbstractEntity> extends IAbstractRepository<E> {

}
