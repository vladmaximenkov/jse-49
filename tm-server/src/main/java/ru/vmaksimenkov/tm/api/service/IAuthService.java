package ru.vmaksimenkov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.dto.UserRecord;
import ru.vmaksimenkov.tm.enumerated.Role;

public interface IAuthService {

    void checkRoles(@Nullable Role... roles);

    @Nullable
    UserRecord getUser();

    @NotNull
    String getUserId();

    boolean isAuth();

    void login(@Nullable String login, @Nullable String password);

    void logout();

    @NotNull
    UserRecord registry(@Nullable String login, @Nullable String password, @Nullable String email);

}
