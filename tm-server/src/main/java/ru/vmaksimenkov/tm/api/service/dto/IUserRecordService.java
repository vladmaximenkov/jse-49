package ru.vmaksimenkov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.dto.UserRecord;
import ru.vmaksimenkov.tm.enumerated.Role;

public interface IUserRecordService extends IAbstractRecordService<UserRecord> {

    @NotNull
    UserRecord create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    UserRecord create(@Nullable String login, @Nullable String password, @NotNull Role role);

    boolean existsByEmail(@Nullable String email);

    boolean existsByLogin(@Nullable String login);

    @Nullable
    UserRecord findByLogin(@Nullable String login);

    void lockUserByLogin(@Nullable String login);

    void removeByLogin(@Nullable String login);

    void setPassword(@NotNull String userId, @Nullable String password);

    void unlockUserByLogin(@Nullable String login);

    void updateUser(@NotNull String userId, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName);

}
