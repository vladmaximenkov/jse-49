package ru.vmaksimenkov.tm.comparator;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.api.entity.IHasStatus;

import java.util.Comparator;

@NoArgsConstructor
public final class ComparatorByStatus implements Comparator<IHasStatus> {

    private static @NotNull
    final ComparatorByStatus INSTANCE = new ComparatorByStatus();

    public static @NotNull ComparatorByStatus getInstance() {
        return INSTANCE;
    }

    @Override
    public int compare(@Nullable final IHasStatus o1, @Nullable final IHasStatus o2) {
        if (o1 == null || o2 == null) return 0;
        if (o1.getStatus() == null) return -1;
        if (o2.getStatus() == null) return 1;
        return o1.getStatus().compareTo(o2.getStatus());
    }

}
