package ru.vmaksimenkov.tm.service;

import lombok.Getter;
import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import ru.vmaksimenkov.tm.api.service.IActiveMQConnectionService;

import javax.jms.Connection;
import java.util.Collections;

import static ru.vmaksimenkov.tm.constant.PropertyConst.URL;

@Getter
public class ActiveMQConnectionService implements IActiveMQConnectionService {

    @NotNull
    public final Connection connection;
    @NotNull
    private final ActiveMQConnectionFactory connectionFactory;

    @SneakyThrows
    public ActiveMQConnectionService() {
        connectionFactory = new ActiveMQConnectionFactory(URL);
        connectionFactory.setTrustedPackages(Collections.singletonList("ru.vmaksimenkov.tm.jms"));
        connection = connectionFactory.createConnection();
        connection.start();
    }

}
