package ru.vmaksimenkov.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.api.repository.dto.IProjectRecordRepository;
import ru.vmaksimenkov.tm.api.repository.dto.ISessionRecordRepository;
import ru.vmaksimenkov.tm.api.repository.dto.ITaskRecordRepository;
import ru.vmaksimenkov.tm.api.repository.dto.IUserRecordRepository;
import ru.vmaksimenkov.tm.api.service.dto.IProjectRecordService;
import ru.vmaksimenkov.tm.api.service.dto.ISessionRecordService;
import ru.vmaksimenkov.tm.api.service.dto.ITaskRecordService;
import ru.vmaksimenkov.tm.api.service.dto.IUserRecordService;
import ru.vmaksimenkov.tm.dto.ProjectRecord;
import ru.vmaksimenkov.tm.dto.SessionRecord;
import ru.vmaksimenkov.tm.dto.TaskRecord;
import ru.vmaksimenkov.tm.dto.UserRecord;
import ru.vmaksimenkov.tm.repository.dto.ProjectRecordRepository;
import ru.vmaksimenkov.tm.repository.dto.SessionRecordRepository;
import ru.vmaksimenkov.tm.repository.dto.TaskRecordRepository;
import ru.vmaksimenkov.tm.repository.dto.UserRecordRepository;

public class AbstractRecordTest extends AbstractTest {

    @NotNull
    protected static final IProjectRecordRepository PROJECT_REPOSITORY = new ProjectRecordRepository(ENTITY_MANAGER);

    @NotNull
    protected static final IProjectRecordService PROJECT_SERVICE = BOOTSTRAP.getProjectService();

    @NotNull
    protected static final ISessionRecordRepository SESSION_REPOSITORY = new SessionRecordRepository(ENTITY_MANAGER);

    @NotNull
    protected static final ISessionRecordService SESSION_SERVICE = BOOTSTRAP.getSessionService();

    @NotNull
    protected static final ITaskRecordRepository TASK_REPOSITORY = new TaskRecordRepository(ENTITY_MANAGER);

    @NotNull
    protected static final ITaskRecordService TASK_SERVICE = BOOTSTRAP.getTaskService();

    @NotNull
    protected static final IUserRecordRepository USER_REPOSITORY = new UserRecordRepository(ENTITY_MANAGER);

    @NotNull
    protected static final IUserRecordService USER_SERVICE = BOOTSTRAP.getUserService();

    @Nullable
    protected static ProjectRecord TEST_PROJECT;

    @Nullable
    protected static SessionRecord TEST_SESSION;

    @Nullable
    protected static TaskRecord TEST_TASK;

    @NotNull
    protected static UserRecord TEST_USER;

}
