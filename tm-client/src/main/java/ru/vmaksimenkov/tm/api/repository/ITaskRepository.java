package ru.vmaksimenkov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.api.IRepository;
import ru.vmaksimenkov.tm.endpoint.TaskRecord;

import java.util.List;

public interface ITaskRepository extends IRepository<TaskRecord> {

    void bindTaskPyProjectId(@NotNull String userId, @NotNull String projectId, @NotNull String taskId);

    boolean existsByName(@NotNull String userId, @NotNull String name);

    boolean existsByProjectId(@NotNull String userId, @NotNull String projectId);

    @Nullable
    List<TaskRecord> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

    @Nullable
    TaskRecord findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    TaskRecord findOneByName(@NotNull String userId, @NotNull String name);

    @Nullable
    String getIdByIndex(@NotNull String userId, @NotNull Integer index);

    void removeAllBinded(@NotNull String userId);

    void removeAllByProjectId(@NotNull String userId, @NotNull String projectId);

    void removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    void removeOneByName(@NotNull String userId, @NotNull String name);

    void unbindTaskFromProject(@NotNull String userId, @NotNull String taskId);

}
