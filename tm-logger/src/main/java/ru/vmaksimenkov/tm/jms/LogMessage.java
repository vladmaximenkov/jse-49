package ru.vmaksimenkov.tm.jms;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

@Getter
@AllArgsConstructor
public class LogMessage implements Serializable {

    @NotNull
    private final String type;

    @NotNull
    private final String event;

    @NotNull
    private final String json;

}
