package ru.vmaksimenkov.tm.constant;

import org.apache.activemq.ActiveMQConnection;
import org.jetbrains.annotations.NotNull;

public class Const {

    @NotNull
    public static final String URL = ActiveMQConnection.DEFAULT_BROKER_URL;

    @NotNull
    public static final String SUBJECT = "JCG_TOPIC";

}